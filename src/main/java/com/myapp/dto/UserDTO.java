package com.myapp.dto;

import com.myapp.domain.User;
import com.myapp.domain.UserStats;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class UserDTO implements Comparable<UserDTO> {

    private final long id;
    private final String email;
    @NonNull
    private final String name;
    private final String avatarHash;
    private final UserStats userStats;
    private final Boolean isFollowedByMe;
    private final double score;

    @Override
    public int compareTo(UserDTO o) {
        if(score == o.getScore())
            return 0;
        return o.getScore() < score ? -1 : 1;
    }

    public double getScore() {
        return score;
    }
}
