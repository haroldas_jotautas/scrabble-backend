package com.myapp.dto;

import com.myapp.domain.Game;
import com.myapp.domain.User;
import lombok.Builder;
import lombok.Value;

/**
 * Created by harol on 2/22/2017.
 */
@Value
@Builder
public class MoveDTO {

    private final long id;
    private final User player;
    private final long gameId;
    private final String move;

    private final boolean error;
    private final String errorMessage;


}
