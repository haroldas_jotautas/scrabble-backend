package com.myapp.dto;

import com.myapp.domain.Move;
import com.myapp.domain.User;
import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Created by harol on 2/22/2017.
 */
@Value
@Builder
public class GameDTO {

    private final long id;
    private final User player1;
    private final User player2;
    private final String board;
    private final String playerDeck;
    private final int turn;
    private final boolean playerTurn;
    private final int player1Points;
    private final int player2Points;
    private final List<Move> player1RecentMoves;
    private final List<Move> player2RecentMoves;
    private final int lettersLeft;

}
