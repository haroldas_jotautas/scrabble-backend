package com.myapp.dto;

import lombok.Builder;
import lombok.Value;

/**
 * Created by harol on 4/20/2017.
 */
@Value
@Builder
public class GameResultDTO {

    private final long id;
    private final boolean error;
    private final String errorMessage;
}
