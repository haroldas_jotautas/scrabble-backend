package com.myapp.dto;

import com.myapp.scrabble.model.Letter;
import lombok.Value;

/**
 * Created by harol on 2/26/2017.
 */
@Value
public class LetterDTO {
    private final int i;
    private final int j;
    private final String letter;

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public String getLetter() {
        return letter;
    }
}
