package com.myapp.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by harol on 2/18/2017.
 */
@Entity
@Table(name = "move")
@Data
@NoArgsConstructor
public class Move {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Setter
    private String word;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Game game;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @NotNull
    @Setter
    @Column(length=65535)
    private String crosschecks;

    @NotNull
    @Getter
    @Setter
    private int points;
}
