package com.myapp.domain;

import com.myapp.scrabble.model.Letter;
import com.myapp.scrabble.model.Rack;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.json.JSONArray;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Random;

/**
 * Created by harol on 2/18/2017.
 */
@Entity
@Table(name = "game")
@Data
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @Setter
    @Getter
    private User user1;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @Setter
    @Getter
    private User user2;

    @NotNull
    @Setter
    @Getter
    @Column(length=65535)
    private String board;

    @NotNull
    @Setter
    @Getter
    @Column(length=65535)
    private String player1Deck;

    @NotNull
    @Setter
    @Getter
    @Column(length=65535)
    private String player2Deck;

    @NotNull
    @Setter
    @Getter
    @Column(length=65535)
    private String pool;

    @Setter
    @Getter
    @NotNull
    private Integer turn;

    @Setter
    @Getter
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private User playerTurn;

    @Setter
    @Getter
    @NotNull
    private Integer player1Points;

    @Setter
    @Getter
    @NotNull
    private Integer player2Points;

    @Setter
    @Getter
    @NotNull
    private Integer botGame;

    @Setter
    @Getter
    @NotNull
    private Integer turnsWithoutWord;

    @Setter
    @Getter
    @NotNull
    private Integer over;

    @ManyToOne(fetch = FetchType.EAGER)
    @Setter
    @Getter
    private User winner;

    public static int DECK_SIZE = 7;

    public Game(){
        this.turnsWithoutWord = 0;
        this.over = 0;

    }

    public void setupNewGame(){
        setEmptyBoard();
        setStartingPool();
        this.player1Deck = "[]";
        this.player2Deck = "[]";
        this.player1Deck = drawDeck(this.player1Deck);
        this.player2Deck = drawDeck(this.player2Deck);
    }

    private String drawDeck(String playerDeck){
        JSONArray deck = new JSONArray(playerDeck);
        JSONArray pool = new JSONArray(this.pool);
        int neededLetters = DECK_SIZE - deck.length();
        for(int i = 0; i < neededLetters; i++){
            if(pool.length() > 0) {
                int randomNumber = generateRandomNumber(0, pool.length() - 1);
                String randomLetter = (String) pool.get(randomNumber);
                deck.put(randomLetter);
                pool.remove(randomNumber);
            }
        }
        this.pool = pool.toString();
        return deck.toString();
    }

    public void drawPlayer1(){
        this.player1Deck = drawDeck(this.player1Deck);
    }

    public void drawPlayer2(){
        this.player2Deck = drawDeck(this.player2Deck);
    }

    public void addTurn(){
        this.turn++;
    }

    public void changePlayerTurn() {
        if(this.playerTurn.getId() == this.user1.getId()){
            this.playerTurn = this.user2;
        }else{
            this.playerTurn = this.user1;
        }
    }

    public void resetTurnsWithoutWord()
    {
        this.turnsWithoutWord = 0;
    }

    public void addToTurnsWithoutWords()
    {
        this.turnsWithoutWord++;
    }

    public void setGameOver(){
        this.over = 1;
        if(this.player1Points > this.player2Points)
        {
            this.winner = this.user1;
        } else if(this.player1Points < this.player2Points) {
            this.winner = this.user2;
        }
    }

    private void setEmptyBoard() {
        this.board = "[\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}],\n" +
                "    [{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"},{\"letter\":\" \"}]\n" +
                "  ]";
    }

    private void setStartingPool(){
        this.pool = "[" +
                "\"*\",\"*\"," +
                "\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\",\"a\"," +
                "\"ą\"," +
                "\"b\"," +
                "\"c\"," +
                "\"č\"," +
                "\"d\",\"d\",\"d\"," +
                "\"e\",\"e\",\"e\",\"e\",\"e\"," +
                "\"ę\"," +
                "\"ė\",\"ė\"," +
                "\"f\"," +
                "\"g\",\"g\"," +
                "\"h\"," +
                "\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\",\"i\"," +
                "\"į\"," +
                "\"y\"," +
                "\"j\",\"j\"," +
                "\"k\",\"k\",\"k\",\"k\"," +
                "\"l\",\"l\",\"l\"," +
                "\"m\",\"m\",\"m\"," +
                "\"n\",\"n\",\"n\",\"n\",\"n\"," +
                "\"o\",\"o\",\"o\",\"o\",\"o\",\"o\"," +
                "\"p\",\"p\",\"p\"," +
                "\"r\",\"r\",\"r\",\"r\",\"r\"," +
                "\"s\",\"s\",\"s\",\"s\",\"s\",\"s\",\"s\",\"s\"," +
                "\"š\"," +
                "\"t\",\"t\",\"t\",\"t\",\"t\",\"t\"," +
                "\"u\",\"u\",\"u\",\"u\"," +
                "\"ų\"," +
                "\"ū\"," +
                "\"v\",\"v\"," +
                "\"z\"," +
                "\"ž\"," +
                "]";
    }

    private int generateRandomNumber(int start, int end){
        Random r = new Random();
        int low = start;
        int high = end;
        if(low == 0 && high == 0){
            return 0;
        }
        return r.nextInt(high-low) + low;
    }

    public String getBotRack() {
        return this.player2Deck;
    }
}
