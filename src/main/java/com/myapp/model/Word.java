package com.myapp.model;

import com.myapp.scrabble.model.Letter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harol on 3/8/2017.
 */
public class Word {

    private List<Letter> wordLetters = new ArrayList<>();
    private int points;
    private PlacedLetter firstLetter;

    public Word(){}

    public void add(int index, Letter letter){
        wordLetters.add(index, letter);
    }

    public void add(Letter letter){
        wordLetters.add(letter);
    }

    public List<Letter> getWordLetters() {
        return wordLetters;
    }

    public void setWordLetters(List<Letter> wordLetters) {
        this.wordLetters = wordLetters;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString(){
        String word = "";
        for(Letter letter : this.wordLetters){
            word += letter.getLetter();
        }
        return word;
    }

    public void setFirstLetter(PlacedLetter firstLetter) {
        this.firstLetter = firstLetter;
    }

    public PlacedLetter getFirstLetter() {
        return firstLetter;
    }
}
