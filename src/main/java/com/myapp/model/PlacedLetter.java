package com.myapp.model;

import com.myapp.dto.LetterDTO;

/**
 * Created by harol on 3/9/2017.
 */
public class PlacedLetter {
    private int i;
    private int j;
    private String letter;
    private boolean wildcard = false;

    public PlacedLetter(){}

    public PlacedLetter(LetterDTO letter) {
        this.i = letter.getI();
        this.j = letter.getJ();


        if(letter.getLetter().contains("*")){
            this.letter = letter.getLetter().replaceAll("\\*", "");
            this.wildcard = true;
        } else {
            this.letter = letter.getLetter();
        }
    }

    public PlacedLetter(int i, int j, String letter) {
        this.i = i;
        this.j = j;
        this.letter = letter;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public String getLetter() {
        return letter;
    }

    public boolean isWildcard() {
        return wildcard;
    }
}
