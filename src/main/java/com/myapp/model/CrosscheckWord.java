package com.myapp.model;

import com.myapp.scrabble.Util.Util;
import com.myapp.scrabble.model.Letter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harol on 3/13/2017.
 */
public class CrosscheckWord {
    private List<Letter> wordLetters = new ArrayList<>();
    private int points;
    private Letter mainLetter;

    public CrosscheckWord(){}

    public void add(int index, Letter letter){
        wordLetters.add(index, letter);
    }

    public void add(Letter letter){
        wordLetters.add(letter);
    }

    public List<Letter> getWordLetters() {
        return wordLetters;
    }

    public void setWordLetters(List<Letter> wordLetters) {
        this.wordLetters = wordLetters;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Letter getMainLetter() {
        return mainLetter;
    }

    public void setMainLetter(Letter mainLetter) {
        this.mainLetter = mainLetter;
    }

    @Override
    public String toString(){
        String word = "";
        for(Letter letter : this.wordLetters){
            word += letter.getLetter();
        }
        return word;
    }

    public void calcValue() {
        for(Letter letter : this.wordLetters){
            if(letter != this.mainLetter)
                this.points += Util.getLetterValue(letter.getLetter());
        }
    }
}
