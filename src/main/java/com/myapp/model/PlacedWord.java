package com.myapp.model;

import com.myapp.scrabble.Util.Annotator;
import com.myapp.scrabble.Util.Util;
import com.myapp.scrabble.model.Letter;
import com.myapp.scrabble.model.Tile;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harol on 3/12/2017.
 */
public class PlacedWord {
    Word word;
    List<CrosscheckWord> crosschecks = new ArrayList<>();
    int points;
    private boolean isWrong;
    private String wrongWord;
    private boolean letterMissing;
    private int sentletters;
    private boolean emptyRackBonus = false;

    public PlacedWord(Word word, List<CrosscheckWord> crosschecks){
        this.word = word;
        this.crosschecks = crosschecks;
    }

    public PlacedWord() {

    }

    public void calcPoints(Tile[][] board){

        int totalValue = 0;
        int tripleWord = 0;
        int doubleWord = 0;
        PlacedLetter firstLetter = word.getFirstLetter();
        int currentLetter = firstLetter.getI();
        for(Letter letter : word.getWordLetters()){
            Tile tile = board[currentLetter][firstLetter.getJ()];
            if(!tile.getLetter().getLetter().isEmpty()){
                int value = Util.getLetterValue(letter.getLetter());
                if(letter.isWildcard())
                    value = 0;

                if(tile.getLetter().isNew()){
                    switch(tile.getMultiplier()){
                        case Tile.DOUBLE_LETTER:
                            totalValue += value*2;
                            break;
                        case Tile.TRIPLE_LETTER:
                            totalValue += value*3;
                            break;
                        case Tile.DOUBLE_WORD:
                            totalValue += value;
                            doubleWord++;
                            break;
                        case Tile.TRIPLE_WORD:
                            totalValue += value;
                            tripleWord++;
                            break;
                        default:
                            totalValue += value;
                            break;
                    }
                }else{
                    totalValue += value;
                }


            }
            currentLetter++;
        }

        totalValue += calculateCrosscheckScores();

        for(int i = 0; i<doubleWord; i++){
            totalValue *= 2;
        }
        for(int i = 0; i<tripleWord; i++){
            totalValue *= 3;
        }

        this.points = totalValue + checkForBonus();
    }

    private int checkForBonus() {
        if(this.emptyRackBonus)
            return 50;

        return 0;
    }

    private int calculateCrosscheckScores(){
        int crosscheckPoints = 0;
        for(CrosscheckWord crosscheckWord : crosschecks){
            crosscheckPoints += crosscheckWord.getPoints();
        }

        return crosscheckPoints;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public List<CrosscheckWord> getCrosschecks() {
        return crosschecks;
    }

    public void setCrosschecks(List<CrosscheckWord> crosschecks) {
        this.crosschecks = crosschecks;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public boolean isWrong() {
        return isWrong;
    }

    public void setWrong(boolean wrong) {
        isWrong = wrong;
    }

    public String getWrongWord() {
        return wrongWord;
    }

    public void setWrongWord(String wrongWord) {
        this.wrongWord = wrongWord;
    }

    public boolean isLetterMissing() {
        return letterMissing;
    }

    public void setLetterMissing(boolean letterMissing) {
        this.letterMissing = letterMissing;
    }

    public int getSentletters() {
        return sentletters;
    }

    public void setSentletters(int sentletters) {
        this.sentletters = sentletters;
    }

    public List<String> getCrosscheckWords(){
        List<String> crosschecks = new ArrayList<>();
        for(CrosscheckWord crosscheckWord : this.crosschecks){
            crosschecks.add(crosscheckWord.toString());
        }
        return crosschecks;
    }

    public void checkWords() {
        List<String> wordsToCheck = new ArrayList<>();
        wordsToCheck.add(this.word.toString());

        for(CrosscheckWord crosscheckWord : this.crosschecks){

            wordsToCheck.add(crosscheckWord.toString());
        }

        for(String word : wordsToCheck){
            word = word.replaceAll("\\*", "");
            if(!Annotator.checkWord(word)){
                this.isWrong = true;
                this.wrongWord = word;
            }
        }
    }

    public boolean isEmptyRackBonus() {
        return emptyRackBonus;
    }

    public void setEmptyRackBonus(boolean emptyRackBonus) {
        this.emptyRackBonus = emptyRackBonus;
    }
}
