package com.myapp.service;

import com.myapp.scrabble.model.Dawg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by harol on 4/26/2017.
 */
@Service
public class ScrabbleServiceImpl implements ScrabbleService {

    private static Dawg dawgEasy;
    private static Dawg dawgHard;

    @Autowired
    public ScrabbleServiceImpl(){

    }

    @Override
    public void init() {
        dawgEasy = new Dawg(Dawg.EASY);
        dawgHard = new Dawg(Dawg.HARD);
    }

    @Override
    public Dawg getDawgEasy() {
        return dawgEasy;
    }

    @Override
    public Dawg getDawgHard() {
        return dawgHard;
    }
}
