package com.myapp.service;

import com.myapp.dto.LetterDTO;
import com.myapp.dto.MoveDTO;
import com.myapp.dto.MoveParams;
import com.myapp.scrabble.model.GameInfo;

import java.util.List;

/**
 * Created by harol on 2/19/2017.
 */
public interface MoveService {
    MoveDTO makeMove(Long gameId, List<LetterDTO> letters);

    void skipTurn(Long gameId);
}
