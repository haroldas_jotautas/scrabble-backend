package com.myapp.service;

import com.google.gson.Gson;
import com.myapp.domain.Game;
import com.myapp.domain.Move;
import com.myapp.domain.User;
import com.myapp.dto.LetterDTO;
import com.myapp.dto.MoveDTO;
import com.myapp.repository.GameRepository;
import com.myapp.repository.MoveRepository;
import com.myapp.repository.UserRepository;
import com.myapp.scrabble.PlayerMove;
import com.myapp.scrabble.model.Dawg;
import com.myapp.scrabble.model.LegalWord;
import com.myapp.scrabble.model.OneMove;
import com.myapp.model.PlacedWord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by harol on 2/19/2017.
 */

@Service
public class MoveServiceImpl implements MoveService {
    private final SecurityContextService securityContextService;
    private final GameRepository gameRepository;
    private final MoveRepository moveRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired private ScrabbleService scrabbleService;

    static Map<String, Dawg> dawgsMap = new HashMap<>();

    public static final String NOT_YOUR_TURN = "Ne tavo eilė....";
    public static final String NOT_A_WORD = "Neteisingas žodis: ";
    public static final String LETTER_MISSING = "Trūksta raidžių.";
    public static final String TOO_SHORT = "Žodis per trumpas.";
    public static final String NO_CONNECT = "Žodis turi jungtis prie ant lentos esančių raidžių.";
    public static final String GAME_OVER = "Žaidimas pasibaigęs.";

    @Autowired
    public MoveServiceImpl(SecurityContextService securityContextService,
                           GameRepository gameRepository,
                           UserRepository userRepository,
                           MoveRepository moveRepository,
                           UserService userService)
    {
        this.securityContextService = securityContextService;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
        this.moveRepository = moveRepository;
        this.userService = userService;
    }

    @Override
    public MoveDTO makeMove(Long gameId, List<LetterDTO> letters){
        final Optional<User> currentUser = securityContextService.currentUser();
        Game game = gameRepository.findOne(gameId);
        final User botUser = userService.getBotUser();

        if(game.getOver() == 1)
        {
            return MoveDTO.builder()
                    .error(true)
                    .errorMessage(GAME_OVER)
                    .build();
        }

        if(game.getPlayerTurn().getId() == currentUser.get().getId()){
            PlacedWord placedWord = PlayerMove.placeWord(game, letters, currentUser.get());

            if(placedWord.isLetterMissing()){
                return MoveDTO.builder()
                        .error(true)
                        .errorMessage(LETTER_MISSING)
                        .build();
            } else if(placedWord.isWrong()){
                return MoveDTO.builder()
                        .error(true)
                        .errorMessage(NOT_A_WORD + placedWord.getWrongWord())
                        .build();
            } else if(placedWord.getSentletters() == placedWord.getWord().getWordLetters().size()
                    && placedWord.getCrosschecks().size() <= 0 && game.getTurn() != 0){
                return MoveDTO.builder()
                        .error(true)
                        .errorMessage(NO_CONNECT)
                        .build();
            } else if(placedWord.getWord().getWordLetters().size() < 2){
                return MoveDTO.builder()
                        .error(true)
                        .errorMessage(TOO_SHORT + placedWord.getWrongWord())
                        .build();
            }

            Move move = addNewMove(game, placedWord, currentUser.get());
            moveRepository.save(move);
            game.addTurn();
            game.resetTurnsWithoutWord();

            gameRepository.save(game);
            game = gameRepository.findOne(gameId);

            if(game.getBotGame() == 1){
                botTurn(moveRepository, game, userService, botUser, scrabbleService);
            } else{
                game.changePlayerTurn();
            }
            gameRepository.save(game);

            return MoveDTO.builder()
                    .id(move.getId())
                    .gameId(gameId)
                    .player(currentUser.get())
                    .build();
        }else{
            return MoveDTO.builder()
                    .error(true)
                    .errorMessage(NOT_YOUR_TURN)
                    .build();
        }

    }

    @Override
    public void skipTurn(Long gameId) {
        final Optional<User> currentUser = securityContextService.currentUser();
        final User botUser = userService.getBotUser();
        Game game = gameRepository.findOne(gameId);

        if(game.getPlayerTurn().getId() == currentUser.get().getId()){

            game.addToTurnsWithoutWords();
            if(game.getTurnsWithoutWord() > 6)
            {
                game.setGameOver();
            }else{
                if(game.getBotGame() == 1){
                    botTurn(moveRepository, game, userService, botUser, scrabbleService);
                } else{
                    game.changePlayerTurn();
                }
            }
            gameRepository.save(game);
        }
    }

    public static void botTurn(MoveRepository moveRepository, Game game, UserService userService, User currentUser, ScrabbleService scrabbleService){
        List<Move> gameMoves = moveRepository.findAllByGame(game);
        OneMove oneMove = new OneMove(game, gameMoves, scrabbleService.getDawgHard());
        LegalWord legalWord = oneMove.generateMove();
        if(legalWord == null)
        {
            game.addToTurnsWithoutWords();
            if(game.getTurnsWithoutWord() > 6)
            {
                game.setGameOver();
            }
        } else {
            PlayerMove.updateScore(game, userService.getBotUser(), legalWord.getWordValue());
            OneMove.checkAndUpdatePool(game, legalWord, userService.getBotUser());
            String boardJson = oneMove.getBoard().getBoardJson();
            game.setBoard(boardJson);
            Move botMove = addBotMove(game, legalWord, currentUser);
            game.resetTurnsWithoutWord();
            moveRepository.save(botMove);
        }
    }

    private Move addNewMove(Game game, PlacedWord placedWord, User user){
        Move newMove = new Move();
        newMove.setGame(game);
        newMove.setUser(user);
        newMove.setWord(placedWord.getWord().toString());
        newMove.setPoints(placedWord.getPoints());

        String crosschecksJson = new Gson().toJson(placedWord.getCrosscheckWords());
        newMove.setCrosschecks(crosschecksJson);

        return  newMove;
    }

    private static Move addBotMove(Game game, LegalWord legalWord, User botUser){
        Move newMove = new Move();
        newMove.setGame(game);
        newMove.setUser(botUser);
        newMove.setWord(legalWord.getPartialWord().getWord());
        newMove.setPoints(legalWord.getWordValue());

        String crosschecksJson = new Gson().toJson(legalWord.getCrosscheckWords());
        newMove.setCrosschecks(crosschecksJson);

        return newMove;
    }
}

