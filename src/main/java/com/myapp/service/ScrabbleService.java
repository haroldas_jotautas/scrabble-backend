package com.myapp.service;

import com.myapp.scrabble.model.Dawg;

/**
 * Created by harol on 4/26/2017.
 */
public interface ScrabbleService {
    void init();
    Dawg getDawgEasy();
    Dawg getDawgHard();
}
