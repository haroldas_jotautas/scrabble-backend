package com.myapp.service;

import com.myapp.domain.Game;
import com.myapp.domain.Move;
import com.myapp.domain.User;
import com.myapp.dto.GameDTO;
import com.myapp.dto.GameResultDTO;
import com.myapp.repository.GameCustomRepository;
import com.myapp.repository.GameRepository;
import com.myapp.repository.MoveRepository;
import com.myapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by harol on 2/18/2017.
 */
@Service
public class GameServiceImpl implements GameService {
    private final SecurityContextService securityContextService;
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final MoveRepository moveRepository;
    private final UserService userService;

    private final GameCustomRepository gameCustomRepository;

    public static final String PLAYER_NOT_FOUND = "Žaidėjas nerastas.";
    public static final String CANT_PLAY_WITH_YOURSELF = "Negali žaisti su savim.";


    @Autowired
    public GameServiceImpl(SecurityContextService securityContextService,
                           GameRepository gameRepository,
                           UserRepository userRepository,
                           GameCustomRepository gameCustomRepository,
                           MoveRepository moveRepository,
                           UserService userService)
    {
        this.securityContextService = securityContextService;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
        this.moveRepository = moveRepository;
        this.gameCustomRepository = gameCustomRepository;
        this.userService = userService;
    }

    @Override
    public Game create() {
        final User currentUser = securityContextService.currentUser().get();

        User user1 = currentUser;
        User user2 = userService.getBotUser();

        Game game = new Game();
        game.setBotGame(1);
        game.setUser1(user1);
        game.setUser2(user2);
        game.setupNewGame();
        game.setTurn(0);
        game.setPlayerTurn(currentUser);
        game.setPlayer1Points(0);
        game.setPlayer2Points(0);
        gameRepository.save(game);
        return game;
    }

    @Override
    public List<GameDTO> getGames() {
        final Optional<User> currentUser = securityContextService.currentUser();
        return gameCustomRepository.findByUser(currentUser.get());
    }

    @Override
    public GameDTO getGame(Long gameId) {
        Game game = gameRepository.findOne(gameId);
        final Optional<User> currentUser = securityContextService.currentUser();
        String deck = null;
        User player1;
        User player2;
        int player1Points;
        int player2Points;
        List<Move> player1RecentMoves;
        List<Move> player2RecentMoves;

        if(currentUser.get() == game.getUser1()){
            deck = game.getPlayer1Deck();
            player1 = game.getUser1();
            player1Points = game.getPlayer1Points();
            player1RecentMoves = moveRepository.findTop3ByGameAndUserOrderByIdDesc(game, game.getUser1());
            player2 = game.getUser2();
            player2Points = game.getPlayer2Points();
            player2RecentMoves = moveRepository.findTop3ByGameAndUserOrderByIdDesc(game, game.getUser2());
        }else if(currentUser.get() == game.getUser2()){
            deck = game.getPlayer2Deck();
            player1 = game.getUser2();
            player1Points = game.getPlayer2Points();
            player1RecentMoves = moveRepository.findTop3ByGameAndUserOrderByIdDesc(game, game.getUser2());
            player2 = game.getUser1();
            player2Points = game.getPlayer1Points();
            player2RecentMoves = moveRepository.findTop3ByGameAndUserOrderByIdDesc(game, game.getUser1());
        } else{
            return null;
        }

        GameDTO gameDTO = GameDTO.builder()
                .id(game.getId())
                .player1(player1)
                .player2(player2)
                .board(game.getBoard())
                .playerDeck(deck)
                .turn(game.getTurn())
                .playerTurn(game.getPlayerTurn().equals(currentUser.get()))
                //.playerTurn(game.getPlayerTurn())
                .player1Points(player1Points)
                .player2Points(player2Points)
                .player1RecentMoves(player1RecentMoves)
                .player2RecentMoves(player2RecentMoves)
                .build();

        return gameDTO;
    }

    @Override
    public List<GameDTO> getGamesInProgress() {
        final Optional<User> currentUser = securityContextService.currentUser();
        return gameCustomRepository.findByUserInProgress(currentUser.get());
    }

    @Override
    public List<GameDTO> getGamesFinished() {
        final Optional<User> currentUser = securityContextService.currentUser();
        return gameCustomRepository.findByUserFinished(currentUser.get());
    }

    @Override
    public GameResultDTO startGame(String playerName) {
        final Optional<User> currentUser = securityContextService.currentUser();
        final Optional<User> opponent = userRepository.findOneByName(playerName);

        if(opponent.isPresent() ){
            if(opponent.get() == currentUser.get()){
                return GameResultDTO.builder()
                        .error(true)
                        .errorMessage(CANT_PLAY_WITH_YOURSELF)
                        .build();
            }
            Game game = gameCustomRepository.findInProgressByPlayers(currentUser.get(), opponent.get());
            if(game != null){
                return GameResultDTO.builder()
                        .id(game.getId())
                        .build();
            }else {
                game = createNewGame(currentUser.get(), opponent.get());
                return GameResultDTO.builder()
                        .id(game.getId())
                        .build();
            }
        }else{
            return GameResultDTO.builder()
                    .error(true)
                    .errorMessage(PLAYER_NOT_FOUND)
                    .build();
        }
    }

    private Game createNewGame(User user1, User user2){
        Game game = new Game();
        game.setBotGame(0);
        game.setUser1(user1);
        game.setUser2(user2);
        game.setupNewGame();
        game.setTurn(0);
        game.setPlayerTurn(user1);
        game.setPlayer1Points(0);
        game.setPlayer2Points(0);
        gameRepository.save(game);
        return game;
    }
}
