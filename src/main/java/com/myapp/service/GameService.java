package com.myapp.service;

import com.myapp.domain.Game;
import com.myapp.dto.GameDTO;
import com.myapp.dto.GameResultDTO;

import java.util.List;

/**
 * Created by harol on 2/18/2017.
 */
public interface GameService {
    Game create();

    List<GameDTO> getGames();

    GameDTO getGame(Long gameId);

    List<GameDTO> getGamesInProgress();

    List<GameDTO> getGamesFinished();

    GameResultDTO startGame(String playerName);
}
