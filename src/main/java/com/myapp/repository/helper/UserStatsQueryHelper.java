package com.myapp.repository.helper;

import com.myapp.domain.QUser;
import com.myapp.domain.UserStats;
import com.querydsl.core.types.ConstructorExpression;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

public class UserStatsQueryHelper {

    public static ConstructorExpression<UserStats> userStatsExpression(QUser qUser) {
        return Projections.constructor(UserStats.class
        );
    }

}
