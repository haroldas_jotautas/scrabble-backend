package com.myapp.repository;

import com.myapp.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by harol on 2/18/2017.
 */

public interface GameRepository extends JpaRepository<Game, Long> {
}

