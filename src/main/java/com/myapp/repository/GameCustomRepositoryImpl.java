package com.myapp.repository;

import com.myapp.Utils;
import com.myapp.domain.Game;
import com.myapp.domain.QGame;
import com.myapp.domain.User;
import com.myapp.dto.GameDTO;
import com.myapp.dto.GameResultDTO;
import com.myapp.service.SecurityContextService;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by harol on 2/8/2017.
 */
public class GameCustomRepositoryImpl implements GameCustomRepository {

    private final JPAQueryFactory queryFactory;
    private final SecurityContextService securityContextService;
    private final MoveRepository moveRepository;

    @Autowired
    public GameCustomRepositoryImpl(JPAQueryFactory queryFactory,
                                    SecurityContextService securityContextService,
                                    MoveRepository moveRepository) {
        this.queryFactory = queryFactory;
        this.securityContextService = securityContextService;
        this.moveRepository = moveRepository;
    }


    @Override
    public List<GameDTO> findByUser(User user) {
        final QGame qGame = QGame.game;
        return queryFactory.select(qGame)
                .from(qGame)
                .where(qGame.user1.eq(user)
                    .or(qGame.user2.eq(user)))
                .orderBy(qGame.id.desc())
                .fetch()
                .stream()
                .map(tuple -> GameDTO.builder()
                    .id(tuple.getId())
                    .player1(tuple.getUser1())
                    .player2(tuple.getUser2())
                    .player1Points(tuple.getPlayer1Points())
                    .player2Points(tuple.getPlayer2Points())
                    .player1RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser1()))
                        .player2RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser2()))
                    .playerTurn(tuple.getPlayerTurn().equals(user))
                        .lettersLeft(Utils.getPoolSize(tuple.getPool()))
                     .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<GameDTO> findByUserFinished(User user) {
        final QGame qGame = QGame.game;
        return queryFactory.select(qGame)
                .from(qGame)
                .where(qGame.user1.eq(user)
                        .or(qGame.user2.eq(user)).and(qGame.over.eq(1)))
                .orderBy(qGame.id.desc())
                .fetch()
                .stream()
                .map(tuple -> GameDTO.builder()
                        .id(tuple.getId())
                        .player1(tuple.getUser1())
                        .player2(tuple.getUser2())
                        .player1Points(tuple.getPlayer1Points())
                        .player2Points(tuple.getPlayer2Points())
                        .player1RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser1()))
                        .player2RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser2()))
                        .playerTurn(tuple.getPlayerTurn().equals(user))
                        .lettersLeft(Utils.getPoolSize(tuple.getPool()))
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<GameDTO> findByUserInProgress(User user) {
        final QGame qGame = QGame.game;
        return queryFactory.select(qGame)
                .from(qGame)
                .where(qGame.user1.eq(user)
                        .or(qGame.user2.eq(user)).and(qGame.over.eq(0)))
                .orderBy(qGame.id.desc())
                .fetch()
                .stream()
                .map(tuple -> GameDTO.builder()
                        .id(tuple.getId())
                        .player1(tuple.getUser1())
                        .player2(tuple.getUser2())
                        .player1Points(tuple.getPlayer1Points())
                        .player2Points(tuple.getPlayer2Points())
                        .player1RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser1()))
                        .player2RecentMoves(moveRepository.findTop3ByGameAndUserOrderByIdDesc(tuple, tuple.getUser2()))
                        .playerTurn(tuple.getPlayerTurn().equals(user))
                        .lettersLeft(Utils.getPoolSize(tuple.getPool()))
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public Game findInProgressByPlayers(User user1, User user2) {
        final QGame qGame = QGame.game;
        return queryFactory.select(qGame)
                .from(qGame)
                .where(qGame.over.eq(1).and(qGame.user1.eq(user1).or(qGame.user1.eq(user2))
                .and(qGame.user2.eq(user1).or(qGame.user2.eq(user2)))))
                .orderBy(qGame.id.desc())
                .fetchOne();
    }
}
