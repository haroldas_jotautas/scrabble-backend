package com.myapp.repository;

import com.myapp.domain.Game;
import com.myapp.domain.Move;
import com.myapp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by harol on 2/18/2017.
 */
public interface MoveRepository extends JpaRepository<Move, Long> {
    List<Move> findTop3ByGameAndUserOrderByIdDesc(Game game, User user);
    List<Move> findAllByGame(Game game);
}
