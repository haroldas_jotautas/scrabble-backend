package com.myapp.repository;

import com.myapp.domain.Game;

import com.myapp.domain.User;
import com.myapp.dto.GameDTO;
import com.myapp.dto.GameResultDTO;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by harol on 2/8/2017.
 */
public interface GameCustomRepository extends Repository<Game, Long> {
    List<GameDTO> findByUser(User user);

    List<GameDTO> findByUserFinished(User user);

    List<GameDTO> findByUserInProgress(User user);

    Game findInProgressByPlayers(User user1, User user2);
}
