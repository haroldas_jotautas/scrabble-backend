package com.myapp;

import com.myapp.service.ScrabbleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DawgInit {

    @Autowired
    private ScrabbleService scrabbleService;

    @PostConstruct
    public void init(){
        scrabbleService.init();
    }
}