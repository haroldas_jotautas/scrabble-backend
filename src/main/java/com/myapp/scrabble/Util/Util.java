package com.myapp.scrabble.Util;

import com.myapp.scrabble.model.Letter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by harol on 10/19/2016.
 */
public class Util {

    public static List<String> ABC = Arrays.asList("a","ą","b","c","č","d","e","ę","ė","f","g","h","i","į","y","j","k","l","m","n","o","p","r","s","š","t","u","ų","ū","v","z","ž");

    public static int getLetterValue(String letter){

        int value = 0;
        switch(letter){
            case "a":
                value = 1;
                break;
            case "ą":
                value = 8;
                break;
            case "b":
                value = 2;
                break;
            case "c":
                value = 10;
                break;
            case "č":
                value = 8;
                break;
            case "d":
                value = 2;
                break;
            case "e":
                value = 1;
                break;
            case "ę":
                value = 10;
                break;
            case "ė":
                value = 4;
                break;
            case "f":
                value = 10;
                break;
            case "g":
                value = 4;
                break;
            case "h":
                value = 10;
                break;
            case "i":
                value = 1;
                break;
            case "į":
                value = 8;
                break;
            case "y":
                value = 5;
                break;
            case "j":
                value = 4;
                break;
            case "k":
                value = 1;
                break;
            case "l":
                value = 2;
                break;
            case "m":
                value = 2;
                break;
            case "n":
                value = 1;
                break;
            case "o":
                value = 1;
                break;
            case "p":
                value = 3;
                break;
            case "r":
                value = 1;
                break;
            case "s":
                value = 1;
                break;
            case "š":
                value = 5;
                break;
            case "t":
                value = 1;
                break;
            case "u":
                value = 1;
                break;
            case "ų":
                value = 6;
                break;
            case "ū":
                value = 8;
                break;
            case "v":
                value = 4;
                break;
            case "z":
                value = 10;
                break;
            case "ž":
                value = 6;
                break;
        }

        return value;
    }

    public static List<Letter> deepCopy(List<Letter> letters){
        List<Letter> copyList = new ArrayList<>();
        for(Letter letter : letters){
            copyList.add(new Letter(letter));
        }

        return copyList;
    }
}
