package com.myapp.scrabble.model;

import com.myapp.scrabble.dawg.ModifiableDAWGSet;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.List;

/**
 * Created by harol on 10/19/2016.
 */
public class Dawg {
    public static final String EASY = "easy";
    public static final String HARD = "hard";

    public static final String PATH_15k = "15klt.txt";
    public static final String PATH_800k = "800k_be_vardu.txt";

    ModifiableDAWGSet dawg;
    private String path;

    private String mode;

    public Dawg(String mode){
        this.mode = mode;
        if(mode.equals(EASY)){
            path = PATH_15k;
        }else if(mode.equals(HARD)){
            path = PATH_800k;
        }
        dawg = readDAWGFromFile();
       // writeDAWGToGraphWiz(dawg);
    }

    public ModifiableDAWGSet getDawg() {
        return dawg;
    }

    public void setDawg(ModifiableDAWGSet dawg) {
        this.dawg = dawg;
    }

    private ModifiableDAWGSet readDAWGFromFile() {
        ModifiableDAWGSet dawg = new ModifiableDAWGSet();

        Resource resource = new ClassPathResource(path);
        InputStream file = null;
        try {
            file = resource.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader in = new BufferedReader(new InputStreamReader(
                file, "Unicode"))) {
            String line;
            while ((line = in.readLine()) != null) {
                if(line.length() > 1)
                    dawg.add(line.toLowerCase());
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dawg;
    }

    private static void writeDAWGToGraphWiz(ModifiableDAWGSet dawg) {
        String graphWiz = dawg.toGraphViz(false, false);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("D:\\Projektai\\Intellij\\DAWG programa\\src\\main\\resources\\graphWiz.txt", "Unicode");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(graphWiz);
        writer.close();
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void addWords(List<String> wordsToAdd) {
        for(String word: wordsToAdd){
            dawg.add(word);
        }
    }
}
