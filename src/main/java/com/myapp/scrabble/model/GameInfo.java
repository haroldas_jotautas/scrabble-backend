package com.myapp.scrabble.model;

/**
 * Created by harol on 12/18/2016.
 */
public class GameInfo {
    int amateurScore;
    int proScore;
    int turns;

    public GameInfo(){}

    public GameInfo(int amateurScore, int proScore, int turns) {
        this.amateurScore = amateurScore;
        this.proScore = proScore;
        this.turns = turns;
    }

    public int getAmateurScore() {
        return amateurScore;
    }

    public void setAmateurScore(int amateurScore) {
        this.amateurScore = amateurScore;
    }

    public int getProScore() {
        return proScore;
    }

    public void setProScore(int proScore) {
        this.proScore = proScore;
    }

    public int getTurns() {
        return turns;
    }

    public void setTurns(int turns) {
        this.turns = turns;
    }
}
