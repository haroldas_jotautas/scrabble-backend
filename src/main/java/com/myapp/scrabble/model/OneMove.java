package com.myapp.scrabble.model;

import com.myapp.domain.*;
import com.myapp.dto.LetterDTO;
import com.myapp.scrabble.PlayerMove;
import com.myapp.service.ScrabbleService;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by harol on 4/5/2017.
 */
public class OneMove {

    Board board;
    LetterPool pool;
    Player player;
    MovesGenerator movesGenerator;
    Dawg dawg;
    int turn;

    public OneMove(com.myapp.domain.Game game, List<Move> gameMoves, Dawg dawg){
        System.out.println("Creating LetterPool..");
        pool = new LetterPool(game.getPool());
        System.out.println("Creating Board..");
        board = new Board(game.getBoard());
        turn = game.getTurn() + 1;
        this.dawg = dawg;
        System.out.println("Creating MovesGenerator..");
        movesGenerator = new MovesGenerator();
        System.out.println("Setting dawgs..");
        createDawgs(gameMoves);
        System.out.println("Creating players..");
        createPlayers(game.getBotRack());

        board.findAnchors();
        board.findCrosschecks(player.getDawg());
    }

    private void createPlayers(String rackJsonString) {
        player = new Player(dawg, rackJsonString);
        player.setName("Botas");
    }

    private void createDawgs(List<Move> gameMoves) {
        //Dawg dawg800 = new Dawg(Dawg.EASY);
        List<String> wordsToAdd = new ArrayList<String>();

        for(Move move : gameMoves){
            String word = move.getWord().replaceAll("\\*", "");
            wordsToAdd.add(word.toLowerCase());
            JSONArray crosschecks = new JSONArray(move.getCrosschecks());
            for(int i = 0; i < crosschecks.length(); i++) {
                String crosscheck = crosschecks.getString(i);
                crosscheck = crosscheck.replaceAll("\\*", "");
                wordsToAdd.add(crosscheck.toLowerCase());
            }
        }
        dawg.addWords(wordsToAdd);
        //dawgsMap.put(Dawg.HARD, dawg800);
    }

    public LegalWord generateMove(){
        movesGenerator.generateMoves(player, board, turn);
        player.sortLegalWordsByValue();
        return player.placeBestScoringWordOnTheBoard(board);
    }

    public Board getBoard() {
        return board;
    }

    public static boolean checkAndUpdatePool(com.myapp.domain.Game game, LegalWord word, User user){
        String pool = PlayerMove.getPoolForPlayer(game, user);

        if(pool != null){
            JSONArray poolJson = new JSONArray(pool);
            for(Letter letterDTO : word.getPartialWord().getLetters()){
                if(letterDTO.isPlaced())
                {
                    String searchFor;
                    if(letterDTO.isWildcard()){
                        searchFor = "*";
                    }else{
                        searchFor = letterDTO.getLetter();
                    }
                    boolean found = false;
                    for (int i = 0; i < poolJson.length(); i++) {
                        if (poolJson.getString(i).equals(searchFor)) {
                            poolJson.remove(i);
                            found = true;
                            break;
                        }
                    }
                    if(!found){
                        return false;
                    }
                }
            }

            game.setPlayer2Deck(poolJson.toString());
            game.drawPlayer2();

        }

        return true;
    }
}
