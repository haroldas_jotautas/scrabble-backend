package com.myapp.scrabble.model;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harol on 10/30/2016.
 */
public class LetterPool {
    private List<Letter> pool = new ArrayList<>();

    public LetterPool(){
        generateLetters();
    }

    public LetterPool(String poolJsonString){
        JSONArray poolJson = new JSONArray(poolJsonString);
        for(int i = 0; i < poolJson.length(); i++) {
            String letter = poolJson.getString(i);
            pool.add(new Letter(letter));
        }
    }

    private void generateLetters() {
        for(int i = 0; i < 2; i++){
            pool.add(new Letter("*"));
        }

        for(int i = 0; i < 12; i++){
            pool.add(new Letter("a"));
        }
        pool.add(new Letter("ą"));
        pool.add(new Letter("b"));
        pool.add(new Letter("c"));
        pool.add(new Letter("č"));
        for(int i = 0; i < 3; i++){
            pool.add(new Letter("d"));
        }
        for(int i = 0; i < 5; i++){
            pool.add(new Letter("e"));
        }
        pool.add(new Letter("ę"));
        for(int i = 0; i < 2; i++){
            pool.add(new Letter("ė"));
        }
        pool.add(new Letter("f"));
        for(int i = 0; i < 2; i++){
            pool.add(new Letter("g"));
        }
        pool.add(new Letter("h"));
        for(int i = 0; i < 13; i++){
            pool.add(new Letter("i"));
        }
        pool.add(new Letter("į"));
        pool.add(new Letter("y"));
        for(int i = 0; i < 2; i++){
            pool.add(new Letter("j"));
        }
        for(int i = 0; i < 4; i++){
            pool.add(new Letter("k"));
        }
        for(int i = 0; i < 3; i++){
            pool.add(new Letter("l"));
        }
        for(int i = 0; i < 3; i++){
            pool.add(new Letter("m"));
        }
        for(int i = 0; i < 5; i++){
            pool.add(new Letter("n"));
        }
        for(int i = 0; i < 6; i++){
            pool.add(new Letter("o"));
        }
        for(int i = 0; i < 3; i++){
            pool.add(new Letter("p"));
        }
        for(int i = 0; i < 5; i++){
            pool.add(new Letter("r"));
        }
        for(int i = 0; i < 8; i++){
            pool.add(new Letter("s"));
        }
        pool.add(new Letter("š"));
        for(int i = 0; i < 6; i++){
            pool.add(new Letter("t"));
        }
        for(int i = 0; i < 4; i++){
            pool.add(new Letter("u"));
        }
        pool.add(new Letter("ų"));
        pool.add(new Letter("ū"));
        for(int i = 0; i < 2; i++){
            pool.add(new Letter("v"));
        }
        pool.add(new Letter("z"));
        pool.add(new Letter("ž"));
    }

    public List<Letter> getPool() {
        return pool;
    }

    public void setPool(List<Letter> pool) {
        this.pool = pool;
    }

    @Override
    public String toString(){
        String pool = "";

        for(Letter letter : getPool()){
            pool += letter.getLetter() + ", ";
        }
        return "" + getPool().size() + ", " + pool;
    }
}
