package com.myapp.scrabble;

import com.myapp.domain.Game;
import com.myapp.domain.User;
import com.myapp.dto.LetterDTO;
import com.myapp.scrabble.model.*;
import com.myapp.model.CrosscheckWord;
import com.myapp.model.PlacedLetter;
import com.myapp.model.PlacedWord;
import com.myapp.model.Word;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harol on 2/26/2017.
 */
public class PlayerMove {

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    public static PlacedWord placeWord(Game game, List<LetterDTO> word, User user){
        List<PlacedLetter> placedLetters = getPlacedLetters(word);
        Board board = new Board(game.getBoard());
        System.out.println("horizontal");
        System.out.println(board);
        System.out.println("vertical");
        System.out.println(board.printVerticalBoard());
        PlacedWord placedWord = new PlacedWord();
        if(!checkAndUpdatePool(game, word, user, placedWord)){
            placedWord.setLetterMissing(true);
            return placedWord;
        }
        placedWord = addWordToBoard(board, placedLetters, placedWord.isEmptyRackBonus());
        placedWord.checkWords();
        placedWord.setSentletters(word.size());
        updateScore(game, user, placedWord.getPoints());
        game.setBoard(board.getBoardJson());
        return placedWord;
    }

    public static boolean checkAndUpdatePool(Game game, List<LetterDTO> word, User user, PlacedWord placedWord){
        String pool = getPoolForPlayer(game, user);

        if(pool != null){
            JSONArray poolJson = new JSONArray(pool);
            for(LetterDTO letterDTO : word){
                String searchFor;
                if(letterDTO.getLetter().contains("*")){
                    searchFor = "*";
                }else{
                    searchFor = letterDTO.getLetter();
                }
                boolean found = false;
                for (int i = 0; i < poolJson.length(); i++) {
                    if (poolJson.getString(i).equals(searchFor)) {
                        poolJson.remove(i);
                        found = true;
                        break;
                    }
                }
                if(!found){
                    return false;
                }
            }

            if(poolJson.length() == 0)
                placedWord.setEmptyRackBonus(true);

            if(user.getId() == game.getUser1().getId()) {
                game.setPlayer1Deck(poolJson.toString());
                game.drawPlayer1();
            }else if(user.getId() == game.getUser2().getId()){
                game.setPlayer2Deck(poolJson.toString());
                game.drawPlayer2();
            }
        }

        return true;
    }

    public static String getPoolForPlayer(Game game, User user){
        String pool = null;
        if(user.getId() == game.getUser1().getId()){
            pool = game.getPlayer1Deck();
        }else if(user.getId() == game.getUser2().getId()){
            pool = game.getPlayer2Deck();
        }
        return pool;
    }

    public static void updateScore(Game game, User user, int points){
        int playerPoints;
        if(user.getId() == game.getUser1().getId()){
            playerPoints = game.getPlayer1Points();
            playerPoints += points;
            game.setPlayer1Points(playerPoints);
        } else {
            playerPoints = game.getPlayer2Points();
            playerPoints += points;
            game.setPlayer2Points(playerPoints);
        }
    }

    private static PlacedWord addWordToBoard(Board board, List<PlacedLetter> word, boolean bonus){
        for(PlacedLetter placedLetter : word){
            Tile tile = board.getHorizontalBoard()[placedLetter.getI()][placedLetter.getJ()];
            if(tile.getLetter().getLetter().isEmpty()){
                Letter newLetter = Letter.getLetter(placedLetter.getLetter().toLowerCase());
                newLetter.setIsNew(true);
                if(placedLetter.isWildcard()){
                    newLetter.setWildcard(true);
                }
                tile.setLetter(newLetter);
            }
        }

        System.out.println("horizontal");
        System.out.println(board);
        System.out.println("vertical");
        System.out.println(board.printVerticalBoard());

        // by direction, we find all new words
        int direction = findDirection(word, board);
        PlacedWord placedWord = getPlacedWords(board, direction, word, bonus);
        return placedWord;
    }

    private static PlacedWord getPlacedWords(Board board, int direction, List<PlacedLetter> word, boolean bonus){
        Word mainWord = getMainWord(board, word, direction);
        List<CrosscheckWord> crosscheckWords = getCrosscheckWords(board, word, direction);

        PlacedWord placedWord = new PlacedWord(mainWord, crosscheckWords);
        placedWord.setEmptyRackBonus(bonus);

        Tile[][] directionalBoard = board.getHorizontalBoard();
        if(direction == HORIZONTAL)
            directionalBoard = board.getVerticalBoard();
        placedWord.calcPoints(directionalBoard);
        return placedWord;
    }

    private static Word getMainWord(Board board, List<PlacedLetter> word, int direction){
        Word mainWord = new Word();
        PlacedLetter firstLetter = findFirstLetter(board, word, direction);
        mainWord.setFirstLetter(firstLetter);

        int iFirstLetter = firstLetter.getI();
        int jFirstLetter = firstLetter.getJ();

        if(direction == VERTICAL) {
            Tile[][] directionalBoard = board.getHorizontalBoard();
            for (int i = iFirstLetter; i < directionalBoard[iFirstLetter].length; i++) {
                Letter currentLetter = directionalBoard[i][jFirstLetter].getLetter();
                if (currentLetter.getLetter().isEmpty())
                    break;

                mainWord.add(currentLetter);
            }
        } else if(direction == HORIZONTAL) {
            Tile[][] directionalBoard = board.getVerticalBoard();
            for (int i = iFirstLetter; i < directionalBoard[iFirstLetter].length; i++) {
                Letter currentLetter = directionalBoard[i][jFirstLetter].getLetter();
                if (currentLetter.getLetter().isEmpty())
                    break;

                mainWord.add(currentLetter);
            }
        }
        return mainWord;
    }

    private static List<CrosscheckWord> getCrosscheckWords(Board board, List<PlacedLetter> word, int direction) {
        List<CrosscheckWord> crosscheckWords = new ArrayList<>();

        Tile[][] directionalBoard = board.getHorizontalBoard();
        if(direction == VERTICAL)
            directionalBoard = board.getHorizontalBoard();

        if(direction == HORIZONTAL)
            directionalBoard = board.getVerticalBoard();

        System.out.println(board.printVerticalBoard());

        for(PlacedLetter newLetter : word){
            CrosscheckWord crosscheckWord = new CrosscheckWord();
            // if tile to the left or right from new letter is not empty, that means we've got to take that word and check it

            int iNewLetter = newLetter.getI();
            int jNewLetter = newLetter.getJ();

            if(direction == HORIZONTAL){
                iNewLetter = newLetter.getJ();
                jNewLetter = newLetter.getI();
            }

            crosscheckWord.setMainLetter(directionalBoard[iNewLetter][jNewLetter].getLetter());

            Letter leftLetter = jNewLetter > 0 ? directionalBoard[iNewLetter][jNewLetter - 1].getLetter() : new Letter();
            Letter rightLetter = jNewLetter < 14 ? directionalBoard[iNewLetter][jNewLetter + 1].getLetter() : new Letter();

            if(!leftLetter.getLetter().isEmpty() || !rightLetter.getLetter().isEmpty()) {
                if(!leftLetter.getLetter().isEmpty()){
                    for(int j = jNewLetter - 1; j >= 0; j--){
                        Letter currentLetter = directionalBoard[iNewLetter][j].getLetter();
                        if(currentLetter.getLetter().isEmpty()){
                            break;
                        }
                        crosscheckWord.add(0, currentLetter);
                    }
                }

                crosscheckWord.add(directionalBoard[iNewLetter][jNewLetter].getLetter());

                if(!rightLetter.getLetter().isEmpty()){
                    for(int j = jNewLetter + 1; j < directionalBoard[iNewLetter].length; j++){
                        Letter currentLetter = directionalBoard[iNewLetter][j].getLetter();
                        if(currentLetter.getLetter().isEmpty()){
                            break;
                        }
                        crosscheckWord.add(currentLetter);
                    }
                }
                crosscheckWord.calcValue();
                crosscheckWords.add(crosscheckWord);
            }
        }


        return crosscheckWords;
    }

    private static PlacedLetter findFirstLetter(Board board, List<PlacedLetter> word, int direction){
        PlacedLetter firstLetter = word.get(0);
        Tile[][] directionalBoard = board.getHorizontalBoard();
        if(direction == VERTICAL) {
            directionalBoard = board.getHorizontalBoard();
            for(PlacedLetter placedLetter : word){
                if(firstLetter.getI() > placedLetter.getI())
                    firstLetter = placedLetter;
            }
        } else if(direction == HORIZONTAL) {
            directionalBoard = board.getVerticalBoard();
            for(PlacedLetter placedLetter : word){
                if(firstLetter.getJ() > placedLetter.getJ())
                    firstLetter = placedLetter;
            }
        }


        int iFirstLetter = firstLetter.getI();
        int jFirstLetter = firstLetter.getJ();

        if(direction == HORIZONTAL){
            iFirstLetter = firstLetter.getJ();
            jFirstLetter = firstLetter.getI();
        }

        firstLetter = new PlacedLetter(iFirstLetter, jFirstLetter,
                directionalBoard[iFirstLetter][jFirstLetter].getLetter().getLetter());

        if(iFirstLetter > 0) {
            Tile letterBeforeFirst = directionalBoard[iFirstLetter - 1][jFirstLetter];

            if (!letterBeforeFirst.getLetter().getLetter().isEmpty()) {
                for (int i = iFirstLetter - 1; i >= 0; i--) {
                    if (directionalBoard[i][jFirstLetter].getLetter().getLetter().isEmpty())
                        break;

                    firstLetter = new PlacedLetter(i, jFirstLetter,
                            directionalBoard[i][jFirstLetter].getLetter().getLetter());
                }
            }

        }


        return firstLetter;
    }

    private static int findDirection(List<PlacedLetter> word, Board board){
        int iCoord = word.get(0).getI();
        int jCoord = word.get(0).getJ();

        boolean horizontal = true;
        boolean vertical = true;

        if(word.size() < 2){

            if((iCoord == 0 || board.getHorizontalBoard()[iCoord - 1][jCoord].getLetter().getLetter().isEmpty())
                    && (iCoord == 14 || board.getHorizontalBoard()[iCoord + 1][jCoord].getLetter().getLetter().isEmpty())){
                vertical = false;
            } else if((jCoord == 0 || board.getHorizontalBoard()[iCoord][jCoord - 1].getLetter().getLetter().isEmpty())
                    && (jCoord == 14 || board.getHorizontalBoard()[iCoord][jCoord + 1].getLetter().getLetter().isEmpty())){
                horizontal = false;
            }
        }else {
            for(int i = 1; i < word.size(); i++){
                if(iCoord != word.get(i).getI()){
                    horizontal = false;
                }
                if(jCoord != word.get(i).getJ()){
                    vertical = false;
                }
            }
        }

        if(horizontal)
            return HORIZONTAL;

        if(vertical)
            return VERTICAL;

        return -1;
    }

    private static List<PlacedLetter> getPlacedLetters(List<LetterDTO> word){
        List<PlacedLetter> placedLetters = new ArrayList<>();
        for(LetterDTO letter : word){
            PlacedLetter placedLetter = new PlacedLetter(letter);
            placedLetters.add(placedLetter);
        }
        return placedLetters;
    }
}
