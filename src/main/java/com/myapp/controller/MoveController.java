package com.myapp.controller;

import com.myapp.domain.Game;
import com.myapp.domain.Move;
import com.myapp.dto.LetterDTO;
import com.myapp.dto.MoveDTO;
import com.myapp.dto.MoveParams;
import com.myapp.scrabble.model.GameInfo;
import com.myapp.service.GameService;
import com.myapp.service.MoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by harol on 2/19/2017.
 */
@RestController
@RequestMapping("/api/moves")
public class MoveController {

    private final GameService gameService;
    private final MoveService moveService;

    @Autowired
    public MoveController(GameService gameService,
                          MoveService moveService) {
        this.gameService = gameService;
        this.moveService = moveService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{gameId}")
    public MoveDTO makeMove(@RequestBody List<LetterDTO> letters, @PathVariable Long gameId) {
        return moveService.makeMove(gameId, letters);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/skip/{gameId}")
    public void skipTurn(@PathVariable Long gameId) {
        moveService.skipTurn(gameId);
    }
}
