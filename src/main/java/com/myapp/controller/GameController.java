package com.myapp.controller;

import com.myapp.domain.Game;
import com.myapp.dto.GameDTO;
import com.myapp.dto.GameResultDTO;
import com.myapp.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by harol on 2/18/2017.
 */
@RestController
@RequestMapping("/api/games")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Game create() {
        return gameService.create();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<GameDTO> getGames() {
        return gameService.getGames();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inProgress")
    public List<GameDTO> getGamesInProgress() {
        return gameService.getGamesInProgress();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/finished")
    public List<GameDTO> getGamesFinished() {
        return gameService.getGamesFinished();
    }

    @RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
    public GameDTO getGame(@PathVariable Long gameId){
        return gameService.getGame(gameId);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/start")
    public GameResultDTO startGame(@RequestBody String playerName) {
        return gameService.startGame(playerName);
    }


}
