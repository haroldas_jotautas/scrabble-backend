# Fussy Backend Springboot

## TODO
### FrontEnd: ###
```
Reg, Login, Home langų dizainas
Atvaizduoti likusių raidžių kiekį
Atvaizduoti informaciją apie žaidėjus (vardas, taškai, paskutiniai ėjimai)
Taškų skaičiavimas
```

### Server: ###
```
Tikrint ar yra tokia raidė deck'e
Notificationai
Žvaigždutės

Tikrinti pirmą ėjimą
Tikrinti ar vientisas žodis
```

## Getting Started

Run Spring Boot.

```
mvn spring-boot:run
```

## Frequently asked questions

* Build becomes an error on IDE with error message "QUser, QRelationship and etc can't be found".
  * Before you open this project from your IDE, you need to build project once with mvn command. Or else, generated source by annotation processor won't be recognized correctly.
```
# It will generate target directory
mvn clean package -DskipTests=true -Dmaven.javadoc.skip=true
# After that, open this project from Intellij IDEA or Eclipse.
```
UTF-8 error. Convert file to UTF-8 in IDE.

## Docker Support

Dev

```bash
mvn clean package -DskipTests=true -Dmaven.javadoc.skip=true
docker build -t IMAGE .
docker run -p 8080:8080 IMAGE
```

Prod

```bash
mvn clean package -DskipTests=true -Dmaven.javadoc.skip=true
docker build --build-arg JASYPT_ENCRYPTOR_PASSWORD=secret -t IMAGE .
docker run -p 8080:8080 \
  -e "SPRING_PROFILES_ACTIVE=prod" \
  -e "MYSQL_ENDPOINT=fussydb.cghjykhuvp1n.us-west-2.rds.amazonaws.com:3306" \
  -e "NEW_RELIC_LICENSE_KEY=d91fdb8e9f80714c147f9eddff98a2300c55d2ea" \
  IMAGE
```